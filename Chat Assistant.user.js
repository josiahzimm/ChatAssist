// ==UserScript==
// @name         Chat Assistant
// @namespace    https://na53.salesforce.com
// @version      0.1
// @description  Notifies you when a Chat is waiting
// @author       Josiah Zimmermann
// @match        https://na53.salesforce.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    shim_GM_notification ()

    function shim_GM_notification () {
        if (typeof GM_notification === "function") {
            return;
        }
        window.GM_notification = function (ntcOptions) {
            checkPermission ();

            function checkPermission () {
                if (Notification.permission === "granted") {
                    fireNotice ();
                }
                else if (Notification.permission === "denied") {
                    alert ("User has denied notifications for this page/site!");
                    return;
                }
                else {
                    Notification.requestPermission ( function (permission) {
                        console.log ("New permission: ", permission);
                        checkPermission ();
                    } );
                }
            }

            function fireNotice () {
                if ( ! ntcOptions.title) {
                    console.log ("Title is required for notification");
                    return;
                }
                if (ntcOptions.text  &&  ! ntcOptions.body) {
                    ntcOptions.body = ntcOptions.text;
                }
                var ntfctn  = new Notification (ntcOptions.title, ntcOptions);

                if (ntcOptions.onclick) {
                    ntfctn.onclick = ntcOptions.onclick;
                }
                if (ntcOptions.timeout) {
                    setTimeout ( function() {
                        ntfctn.close ();
                    }, ntcOptions.timeout);
                }
            }
        }
    }
    var i = setInterval(function() {
        var chatWindow = document.getElementById("liveAgentChatRequestList");
        if (chatWindow !== null) {
            var chats = chatWindow.childNodes.length;
            var details = {
                    text:       chats+" Chat(s) Waiting.",
                    title:      'Chats are Waiting',
                    timeout:    6000,
                    onclick:    function () {
                        console.log ("Notice clicked.");
                        window.focus ();
                    }
                };
            if (chats !== 0) {
                GM_notification(details);
            };
        };
    }, 10000);
})();